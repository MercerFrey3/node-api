## Requiremenets
[Minikube](https://minikube.sigs.k8s.io/docs/start/)  
[Helm](https://helm.sh/docs/intro/install/)  
[Kubectl](https://kubernetes.io/docs/tasks/tools/)

## Steps to Build App
Write the following commands to create the same setup cluster.
```
minikube start
minikube addons enable ingress 
```
Create an agent or use the existing one. I can provide the agent access token.
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install dev-gitlab-agent gitlab/gitlab-agent \
    --set image.tag=v15.7.0 \
    --set config.token=iZFQNb6hpsF39Gh9QGcpH64HxgzpDAhRgtfpBecN_Lj7gjzAsA \
    --set config.kasAddress=wss://kas.gitlab.com
```
[This agent](https://docs.gitlab.com/ee/user/clusters/agent/gitops.html) checks the dev-deployment. It is pull based and Gitlab recommends this workflow. This agent copys the node-api-chart from the dev-deployment and installs it in the minikube cluster. This agent provides us to store all of our infrastructure in Git as a part of the Gitops paradigm. 


The app is working now. You can access it with this command.
```
kubectl get ingress
```

Check the Address on your browser. *\<address>/food*

## CI/CD Pipeline

This small script is necessary because it gives cluster-admin clusterrole to the all service accounts in the cluster. This is [strongly discouraged](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#service-account-permissions). I will restrict these permissions in the future.   
```
kubectl create clusterrolebinding serviceaccounts-cluster-admin \
  --clusterrole=cluster-admin \
  --group=system:serviceaccounts
```

When the agent runs it creates a runner automatically in the cluster. It connects to the Gitlab. You can check that runner's connection from  
*Project -> Settings -> CI/CD -> Runners -> Specific runners.* 

When you make an update in the main branch, the pipeline runs ([Push-based](https://docs.gitlab.com/ee/user/clusters/agent/ci_cd_workflow.html)) inside the kubernetes cluster with the Gitlab Runner. It has two stages.

- Build: This stage builds a docker image on the Gitlab Runner. It creates docker image with the $CI_COMMIT_SHORT_SHA} tag.  
    ```
    - until docker info; do sleep 1; done
    ```
    This command is necessary because of this [issue](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27215/?_gl=1*1vz7v6m*_ga*MTMyNTU3MjIyNy4xNjcwNjExMzQ3*_ga_ENFH3X7M5Y*MTY3MTM5MjI4Ni4xNy4wLjE2NzEzOTIyODYuMC4wLjA.). It cost me a lot of hours.
- Deploy: Deploy stage runs helm upgrade command with the --set web.image.tag=${CI_COMMIT_SHORT_SHA} parameter. This parameter provides us to pull the docker image that pushed from the build stage.

## Helm Chart
The helm chart is in the dev-deployment branch. You can access the helm chart with the following command.
```
git clone https://gitlab.com/MercerFrey3/node-api.git  
git checkout dev-deployment
```
If you wish to run app directly:
```
helm install node-api node-api-chart/
```

node-api-chart directory has the helm structure.
- **node-api-deployment.yml:** Simple Nodejs deployment.
- **node-api-service.yml:** Simple Nodejs service.
- **node-api-db-deployment.yml:** Postgresql database deployment. 
- **node-api-db-service.yml:** Simple Postgresql service.
- **ingress-service.yml:** It directs every request to the Nodejs Service. There is no endpoint for the database. The endpoint for the database can be created easily.
- **values.yaml** has name, image, service and replica fields for the node-api and node-api-db. **NODE_ENV: development** is important because it changes the environment variable of the node-api(Nodejs deployment). We can differentiate the same application in different environments by setting this variable.  

- **runner-manifest.ymal:** It creates a Gitlab runner that enables us to run CI/CD pipeline inside the Kubernetes cluster. Gitlab provides limited number of CI/CD minutes for the Shared Runners. Self-hosted Runner is free and does not have time quota. Since this runner is installed via Gitlab Agent(inside the kubernetes cluster), it makes it easier to run pipeline by not requiring the kubernetes context.

There are two more yaml files outside the helm chart. 
- runner-values.yml: We create runner-manifest.yaml with the help of this yml file. It contains the values for the Gitlab Runner. It is necessary to create if we want to create [Runner with the Gitlab Agent](https://docs.gitlab.com/runner/install/kubernetes-agent.html). It is used as temporary values file for runner-manifest.yml.
```
helm template gitlab-runner -f runner-values.yaml gitlab/gitlab-runner > runner-manifest.yaml
```   
- clusterrolebinding.yml: It does not work at the moment. In the future it will be used to provide necessary permissions to the default service account. Currently, the small script under the CI/CD header solves this issue. However, it is not an optimal solution.

## Development and Production Environments
We will follow [Gitlab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html). **main** is the development branch. After a release, it will be merged to the **production** branch. The CI/CD pipeline for the main works with the images that contains hashed tags. It also 
```
--set web.NODE_ENV=development  
```
sets the NODE_ENV for the development. It is global variable for the node-api.

The CI/CD pipeline for the production works with specified verions as tags. It also 
```
--set web.NODE_ENV=production  
```
sets the NODE_ENV for the production. It is global variable for the node-api.

## Dockerfile and docker-compose.yml
Dockerfile is created for the Nodejs application. db.Dockerfile is created for the postgresql database with initial values. They are pushed on the mercerfrey/node_api and mercerfrey/node_api_db respectively. CI/CD pipeline builds and pushes only the mercerfrey/node_api image.  

## Pipeline Status Emails
I used a project integration: [Pipeline Status Emails](https://docs.gitlab.com/ee/user/project/integrations/pipeline_status_emails.html). It notifies only the broken images via my e-mail. 

## Future Work
- Edit permissions to the service accounts. They can access to everything right now.
- Add volume to the database.
- Add kubernetes secrets to hide some important informations. Like database password or gitlab runner registration token. 
- Add namespaces.
- Run production environment on the GCP EKS.  
- Add tag to the Gitlab Runners. So that the CI/CD pipeline can run with on the specific Gitlab Runner. For example, production stage runs with its own runner, and the development stage runs on its own. 